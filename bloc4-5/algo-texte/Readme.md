Algorithmes du texte
====================

* Recherche textuelle - Algorithmique du texte
  - [support de présentation](algo-texte.pdf)

* Algorithmes de Knuth, Morris et Pratt et de Boyer Moore  
  calepin – support de « TD sur machine »  
  * [tp-recherche-textuelle.ipynb](tp-recherche-textuelle.ipynb)
  * ou en ligne via le serveur `jupyter.fil.univ-lille1.fr`
    accessible à
    [frama.link/diu-ipynb-recherche-text](https://frama.link/diu-ipynb-recherche-text)
  * disponible sous forme d'un
	[fichier PDF](tp-recherche-textuelle.pdf) ou d'un
	[source Python](tp-recherche-textuelle.py)

* Automates pour la recherche textuelle  
  calepin – support de « TD sur machine » 
  * [tp-automates.ipynb](tp-automates.ipynb)
  * ou en ligne via le serveur `jupyter.fil.univ-lille1.fr`
    accessible à
    [frama.link/diu-ipynb-automate](https://frama.link/diu-ipynb-automate)
  * disponible sous forme d'un
	[fichier PDF](tp-automates.pdf) ou d'un
	[source Python](tp-automates.py)

* matériel complémentaire
  - bibliothèque [Automata.py](./Automata.py) pour la manipulation d'automates
  - les bibliothèques *graphviz*, *matplotlib*, *timeit* et le
    programme`graphviz` sont également nécessaires pour visualiser
	correctement les figures.
  - les images [kmp_shift_small.jpg](kmp_shift_small.jpg)
    et [kmp_shift.png](kmp_shift.png)
	utilisées dans le calepin recherche textuelle 
  - jeux de données
	- [les-miserables.txt](./les-miserables.txt)
	- [2019-nCoV_WH01.fa](./2019-nCoV_WH01.fa)
	  (déjà utilisé lors du TP programmation dynamique)

