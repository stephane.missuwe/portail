Graphes
=======

Travaux dirigés

* sujet [graphe1.md](graphe1.md) - [version PDF](graphe1.pdf)
* fichier [dico.py](src/dico.py)
* une [proposition de code](src/ours-cage.py) pour la résolution et le parcours de graphe en largeur ou en profondeur, avec application *ours-cage*

Travaux pratiques

* notebook jupyter [graphe.ipynb](graphe.ipynb) 
  - et les figures associées [./fig/*](./fig/)
  - également en ligne via le serveur `jupyter.fil.univ-lille1.fr`
	accessible via
	[frama.link/diu-ipynb-graphe](https://frama.link/diu-ipynb-graphe) 
