Calendrier
==========


[DIU Enseigner l'informatique au lycée](./Readme.md)

* [Calendrier blocs 1 à 3, 2020/21](calendrier-b1-3-2022.md) pour mémoire

Sommaire 

[[_TOC_]]

Juin/juillet 2022
=================

mardi 28 juin 2022
------------------

### Découverte du langage SQL ###

- [travaux pratiques](bloc4-5/sql1/Readme.md)
    - éléments de correction :
      [reponses_exo1.sql](bloc4-5/sql1/reponses_exo1.sql) et
      [reponses_exo2.sql](bloc4-5/sql1/reponses_exo2.sql)
- [support de cours](bloc4-5/sql1-cours/cours_sql1.md)

| quand       | qui      | quoi  | où         | avec qui   |
| ----------- | -------- | ----- | ---------- | ---------- |
| 9h-11h      | groupe A | TP    | M5-A14     | Bruno Bgrt |
|             | groupe B |       | M5-A15     | Patricia   |
|             | groupe C |       | M5-A16     | Patrice    |
| 11h15-12h15 | tous     | cours | M5-Bacchus | Maude      |


### Structure de données liste ###

- [cours-TP](bloc4-5/listes/readme.md) 
    - [éléments de solution](bloc4-5/listes/sol/elementsdesolution.md)

| quand       | qui      | où     | avec qui  |
| ----------- | -------- | ------ | --------- |
| 13h15-16h30 | groupe A | M5-A14 | Francesco |
|             | groupe B | M5-A15 | Philippe  |
|             | groupe C | M5-A16 | Patrice   |

mercredi 29 juin 2022
---------------------

### Requêtes SQL en Python ###

- [travaux pratiques](bloc4-5/sql2/Readme.md)
    - éléments de correction :
      [sql2_partie1.py](bloc4-5/sql2/sql2_partie1.py) et 
      [sql2_partie2.py](bloc4-5/sql2/sql2_partie2.py) 

- [compléments](bloc4-5/sql+/Readme.md) : 
  - accès aux BD depuis un langage de programmation 
  - exemple d'injection SQL 

| quand        | qui      | où     | avec qui   |
| ------------ | -------- | ------ | ---------- |
| 9h-11h       | groupe A | M5-A14 | Bruno Bgrt |
|              | groupe B | M5-A15 | Patricia   |
|              | groupe C | M5-A16 | Maude      |

### Types de données abstrait et structures de données ###

- cours : 
  [sur le portail](bloc4-5/sd/readme.md) /
  [version PDF](bloc4-5/sd/structures-donnees.pdf)

| quand       | qui  | où         | avec qui |
| ----------- | ---- | ---------- | -------- |
| 11h15-12h15 | tous | M5-Bacchus | Benoit   |

### Utilisation de piles et files ###

- [travaux pratiques](bloc4-5/pile_file/pile.md)

| quand       | qui      | où     | avec qui      |
| ----------- | -------- | ------ | ------------- |
| 13h15-15h15 | groupe A | M5-A14 | Philippe      |
|             | groupe B | M5-A15 | Benoit        |
|             | groupe C | M5-A16 | Jean-Stéphane |

### Informatique et partage ###

- support de présentation 
  - [version PDF pour impression](/bloc3/libre/partage-papier.pdf), 
    [version PDF pour écran](bloc3/libre/partage-ecran.pdf)
  
| quand       | qui  | où         | avec qui       |
| ----------- | ---- | ---------- | -------------- |
| 15h30-16h30 | tous | M5-Bacchus | Bruno Beaufils | 

jeudi 30 juin 2022
------------------

### Conception de bases de données relationnalles ###

- [cours](bloc4-5/sql2-cours/cours_sql2.md)
- [travaux dirigés et pratiques](bloc4-5/sql3/Readme.md)

| quand       | qui      | où         | avec qui |
| ----------- | -------- | ---------- | -------- |
| 9h-10h      | tous     | M5-Bacchus | Patricia |
| 10h15-12h15 | groupe A | M5-A14     | Patricia |
|             | groupe B | M5-A15     | Maude    |
|             | groupe C | M5-A16     | Benoit   |

### Programmation Orientée Objet ###

- [cours, 1re partie](bloc4-5/poo/readme.md) 

| quand       | qui  | où         | avec qui        |
| ----------- | ---- | ---------- | --------------- |
| 13h15-14h30 | tous | M5-Bacchus | Jean-Christophe |

### Arbres binaires ###

- [travaux dirigés](bloc4-5/arbres/Readme.md)

| quand       | qui      | où    | avec qui      |
| ----------- | -------- | ----- | ------------- |
| 14h45-16h30 | groupe A | M5-A7 | Jean-Stéphane |
|             | groupe B | M5-A8 | Bruno Bgrt    |
|             | groupe C | M5-A9 | Philippe      |

vendredi 1er juillet 2022
-------------------------

### Programmation Orientée Objet ###

- [cours, suite et fin](bloc4-5/poo/readme.md)

| quand  | qui  | où         | avec qui        |
| ------ | ---- | ---------- | --------------- |
| 9h-10h | tous | M5-Bacchus | Jean-Christophe |

### Arbres binaires et arbres de recherche ###

- [travaux pratiques](bloc4-5/arbres/Readme.md)

| quand       | qui      | où     | avec qui      |
| ----------- | -------- | ------ | ------------- |
| 10h15-12h15 | groupe A | M5-A14 | Jean-Stéphane |
|             | groupe B | M5-A15 | Bruno Bgrt    |
|             | groupe C | M5-A16 | Philippe      | 
|             |          |        |               |
| 13h15-16h30 | groupe A | M5-A14 | Jean-Stéphane |
|             | groupe B | M5-A15 | Benoit        |
|             | groupe C | M5-A16 | Philippe      | 

Décembre 2022
=============

à venir...

<!-- eof -->
