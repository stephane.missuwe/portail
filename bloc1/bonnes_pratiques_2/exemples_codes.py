import random
#################################################################

def creer_grille_vierge(nbLignes,nbColonnes):
    '''
    envoie une liste de listes
    correspondant à une grille du jeu de la vie aux dimensions souhaitées, ne
    contenant aucune cellule
    : param  nbLignes : (int) entier indiquant le nombre de lignes
    : param  nbColonnes : (int) entier indiquant le nombre de colonnes
    :Exemple : >>> creer_grille_vierge(3,2)
    [[[0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]]]
    '''
    liste = []
    for l in range(0,nbLignes): # crée les lignes
        liste.append([])
        for _ in range(0,nbColonnes): # crée les lignes
            liste[l].append([0]*3) # crée [0,0,0]
    return liste
#creer_grille_vierge(5,8)

# devient

def creer_grille_vierge_bis(nbLignes,nbColonnes):
    return [ [ [0,0,0] for ligne in range(nbColonnes)] for _ in range(nbLignes) ]
#creer_grille_vierge_bis(5,8)

#################################################################

def creerMer(nL, nC):

    gr = []
    for i in range(nL):
        uneLigne = [0]*nC
        gr.append(uneLigne)
    return gr

# devient

def creerMer_bis(nL,nC):
    #return [ [0]*nC for _ in range(nL) ]
    return [ [0 for _ in range(nC)] for _ in range(nL)]

#################################################################


def comportement_requin(l,lig,col):
    # ---------DEPLACEMENT ----------
    l[lig][col][2] = l[lig][col][2] - 1
    #if l_dest == 0 and c_dest == 0: # s'il n'existe aucune case voisine avec un thon
    if len(voisins_case(l,col,lig,1)) != 0: # si on peut se déplacer sur une case avec un thon
        l_dest,c_dest = calcul_deplacement(voisins_case(l,col,lig,1))
        tps_gest = l[lig][col][1]
        l[lig + l_dest][col + c_dest] = [2,tps_gest-1,ENERGIE]
        # suppression dans la case d'origine
        l[lig][col] = [0,0,0]
    elif len(voisins_case(l,col,lig,0)) != 0: # sinon si on peut se déplacer sur une case vide
        l_dest,c_dest = calcul_deplacement(voisins_case(l,col,lig,0)) # on choisit une case vide
        tps_gest = l[lig][col][1]
        energie = l[lig][col][2]
        l[lig + l_dest][col + c_dest] = [2,tps_gest-1,energie]
        # suppression dans la case d'origine
        l[lig][col] = [0,0,0]     
    else : # pas de déplacement il faut diminuer la gestation
        l_dest,c_dest = 0,0
        tps_gest = l[lig][col][1]
        energie = l[lig][col][2]
        l[lig][col] = [2,tps_gest-1,energie]
    # ---------ENERGIE ----------    
    if l[lig + l_dest][col + c_dest][2] == 0: # si le niveau d'énergie tombe à 0
        l[lig + l_dest][col + c_dest] = [0,0,0] # le requin meurt
    else:
        # ---------REPRODUCTION ---------- 
        if l[lig + l_dest][col + c_dest][1] == 0:
            # reproduction seulement si il y a eu déplacement
            if(l_dest,c_dest) != (0,0): 
                l[lig][col] = [2,TPS_GEST_REQUIN,ENERGIE]
                l[lig + l_dest][col + c_dest][1] = TPS_GEST_REQUIN
            else: # réinitialisatiojn gestation
                l[lig + l_dest][col + c_dest][1] = TPS_GEST_REQUIN
    return l

# devient

def comportement_requin_refactor(l,lig,col):    
    requin = l[lig][col]
    decremente_energie(requin)
    # fournit le déplacement du requin après déplacement (ou pas si 0,0 )
    l_dest,c_dest = deplacement_requin(l,lig,col)
    if requin_est_mort(l[lig + l_dest][col + c_dest]):
        l[lig + l_dest][col + c_dest] = [0,0,0] # le requin meurt
    else:
        reproduction_requin(l,lig,col,l_dest,c_dest)
    return l

def decremente_energie(requin):
    requin[2] = requin[2] - 1

def requin_est_mort(requin):
    return requin[2] == 0
def deplacement_requin(l,lig,col):
    requin = l[lig][col]
    #if l_dest == 0 and c_dest == 0: # s'il n'existe aucune case voisine avec un thon
    if len(voisins_case(l,col,lig,1)) != 0: # si on peut se déplacer sur une case avec un thon
        l_dest,c_dest = calcul_deplacement(voisins_case(l,col,lig,1))
        tps_gest = requin[1]
        l[lig + l_dest][col + c_dest] = [2,tps_gest-1,ENERGIE]
        # suppression dans la case d'origine
        l[lig][col] = [0,0,0]
    elif len(voisins_case(l,col,lig,0)) != 0: # sinon si on peut se déplacer sur une case vide
        l_dest,c_dest = calcul_deplacement(voisins_case(l,col,lig,0)) # on choisit une case vide
        tps_gest = requin[1]
        energie = requin[2]
        l[lig + l_dest][col + c_dest] = [2,tps_gest-1,energie]
        # suppression dans la case d'origine
        l[lig][col] = [0,0,0]     
    else : # pas de déplacement il faut diminuer la gestation
        l_dest,c_dest = 0,0
        tps_gest = requin[1]
        energie = requin[2]
        l[lig][col] = [2,tps_gest-1,energie]
    return l_dest,c_dest

def reproduction_requin(l,lig,col,l_dest,c_dest):
    if l[lig + l_dest][col + c_dest][1] == 0:
            # reproduction seulement si il y a eu déplacement
            if(l_dest,c_dest) != (0,0): 
                l[lig][col] = [2,TPS_GEST_REQUIN,ENERGIE]
                l[lig + l_dest][col + c_dest][1] = TPS_GEST_REQUIN
            else: # réinitialisatiojn gestation
                l[lig + l_dest][col + c_dest][1] = TPS_GEST_REQUIN

# mais aussi 



def nombrePoissons(gr, typeP):
    """
    Compte le nombre de poissons pour le type demandé.
    :param gr: (liste de listes) une grille de la mer
    :param typeP: (string) THON ou REQUIN
    :return: (int) le nombre de poisson du typeP

    """
    nbP = 0
    for l in gr:
        for c in l:
            if c != 0:
                if c['type'] == typeP:
                    nbP += 1
    return nbP

def nombrePoissons_bis(gr, typeP):
    """
    ...
    """
    return len ( [ 1 for l in gr for c in l if c != 0 and c['type'] == typeP ]  )

def nombrePoissons_ter(grille, typePoisson):
    """
    ...
    """
    return len ( [ 1 for ligne in grille for case in ligne if case != 0 and c['type'] == typePoisson ]  )


def creer_grille(n,p):
    """
    """
    valeur = [0]
    L=[]
    for _ in range(n):
        L.append([valeur]*p)
    return L



if __name__ == '__main__':
    import doctest
    doctest.testmod()