Une aventure de Bob Morane
==========================

### Résumé ###

> Bob Morane a mis la main sur une série de messages cryptés de l'Ombre
jaune, son féroce ennemi. Il vous a envoyé ces messages, que vous pouvez
visualiser dans la plupart des éditeurs de texte modernes. Apparemment,
ils sont composés d'idéogrammes, sans doute chinois, mais une analyse
plus poussée vous convainc qu'ils n'ont aucune signification.
>
> Il s'agit sans doute d'un message codé !

Quelques petits rappels sur le codage moderne des caractères
------------------------------------------------------------

Le premier codage informatique des caractères a été le code ASCII, qui
permettait de coder 127, puis 255 caractères. Si c'est suffisant pour
coder l'alphabet occidental standard et ses dérivés proches (caractères
accentués, quelques caractères spécifiques à chaque langue, ...), c'est
notoirement insuffisant pour coder tous les symboles de toutes les
langues possibles, notamment les langues utilisant les idéogrammes.

C'est pour résoudre ce problème qu'a été définie la norme UNICODE et le
codage compatible UTF-8.

La norme Unicode assigne une valeur comprise entre 0 et 1.114.141 (entre
0 et 10FFFF en hexadécimal) à chaque caractère. On parle de *point de
code*.

Le codage UTF-8 code ces valeurs sur plusieurs octets (entre 1 et 4),
selon la valeur du codage Unicode considéré. Le premier octet du codage
en UTF-8 nous indique sur combien d'octets il sera codé.

| Début | Fin       | Octet 1  | Octet 2  | Octet 3  | Octet 4  | Bits utiles |
| ----- | --------- | -------- | -------- | -------- | -------- | ----------- |
| 0     | 127       | 0xxxxxxx |          |          |          | 7           |
| 128   | 2047      | 110xxxxx | 10xxxxxx |          |          | 11          |
| 2048  | 65535     | 1110xxxx | 10xxxxxx | 10xxxxxx |          | 16          |
| 65536 | 1.114.141 | 11110xxx | 10xxxxxx | 10xxxxxx | 10xxxxxx | 21          | 

Quelques exemples :

1.  Le caractère `A` a pour code ASCII 65, son codage UTF-8 sera `0`1000001. A savoir :
    -   Bit de poids fort à 0 : Le caractère est codé sur un bit
    -   Les sept bits restants représentent la valeur 65.
2.  La lettre ASCII étendu <img src="images/u00e3.png" height="20"> a comme valeur Unicode
    $`\overline{\mathtt{00E3}\raisebox{3mm}{}}_{16}`$
    -   Soit en binaire : 1110 0011
    -   Codé en UTF-8 sur 2 octets :
        ```math
        \overbrace{{\bf \scriptstyle 110}0}^C\,\overbrace{0011}^3 \,\overbrace{{\bf  \scriptstyle 10}10}^A\, \overbrace{0011}^3
        ``` 
    -   Cette lettre n'est pas codée sur un octet, sinon il y aurait
        confusion possible avec la convention de codage en UTF-8 (son
        bit de poids fort vaut 1)
3.  Le caractère arabe <img src="images/u06fa.png" height="20"> correspond au *point de code*
    $`\overline{\mathtt{06FA}\raisebox{3mm}{}}_{16}`$
    -   Soit en binaire : 0110 1111 1010
    -   Codé en UTF-8 sur 2 octets :
        ```math
        \overbrace{{\bf \scriptstyle 110}1}^D\,\overbrace{1011}^B \,\overbrace{{\bf  \scriptstyle 10}11}^B\, \overbrace{1010}^A
        ```
4.  Le caractère gurmukhi <img src="images/u0A23.png" height="20"> a pour valeur Unicode
    $`\overline{\mathtt{0A23}\raisebox{3mm}{}}_{16}`$
    -   Soit en binaire : 0000 1010 0010 0011
    -   Codé en UTF-8 sur 3 octets :
        ```math
        \overbrace{\bf \scriptstyle 1110}^E\,\overbrace{0000}^0 \,\overbrace{{\bf  \scriptstyle 10}10}^A\, \overbrace{1000}^8 \,\overbrace{{\bf  \scriptstyle 10}10}^A\, \overbrace{0011}^3
        ```
5.  L'idéogramme <img src="images/u28403.png" height="20"> correspond au *point de code*
    $`\overline{\mathtt{28403}\raisebox{3mm}{}}_{16}`$
    -   Soit en binaire : 0 0010 1000 0100 0000 0011
    -   Codé en UTF-8 sur 4 octets :
        ```math
        \overbrace{\bf \scriptstyle 1111}^F\,
        \overbrace{{\bf  \scriptstyle 0}{\color{red}0}00}^0 \,
        \overbrace{{\bf  \scriptstyle 10}10}^A\,
        \overbrace{1000}^8\,
        ```
        ```math
        \overbrace{{\bf  \scriptstyle 10}01}^9\,
        \overbrace{0000}^0\,
        \overbrace{{\bf  \scriptstyle 10}00}^8\,
        \overbrace{0011}^3\,
        ```

Les messages cryptés de l'Ombre jaune
-------------------------------------

Le répertoire [`moranechinois`](./moranechinois/) contient les messages cryptés de l'Ombre jaune que vous a envoyés Bob Morane.

* visualisez quelques-uns de ces messages avec votre éditeur de texte... C'est du chinois !
* utilisez une commande comme `hexedit` pour en visualiser le contenu en hexadécimal. 

Vous êtes en charge de décrypter un de ces messages. 

En Python
---------

Le document [« Rappels (compléments...) Python »](../binary/python.md) présente quelques rappels Python qui seront utiles :

* Entiers littéraux en Python
* Opérations logiques sur les entiers (opérations bit à bit)

Nous utiliserons également le module [`binary_IO`](../binary/binary_IO.py) qui définit deux classes nommées `Reader` et `Writer` dont les objets permettent de lire et écrire des données binaires dans des fichiers.

Questions
---------

**Question 1.** Si on suppose que les messages de l'Ombre Jaune sont codés en UTF-8, sur combien d'octets est codé chaque idéogramme ?

**Question 2.**

1.  Dans le codage d'un idéogramme de ce message, combien de bits portent réellement de l'information (liée à l'idéogramme)?
2.  Combien de codages ASCII peut-on y cacher ?

**Question 3.** Testez l'hypothèse qui n'a pas manqué de vous apparaître au fil des questions précédentes :
-   Écrivez une fonction qui prend en entrée trois octets représentant
    un idéogramme (cas 4 des exemples ci-dessus) et qui en extrait les
    octets intéressants.
-   Écrivez une fonction qui ouvre un fichier de message codé, et qui
    retourne le message décrypté sous forme d'une chaîne de caractères.

**Question 4.** Maintenant, dans l'autre sens : écrivez une fonction qui prend en entrée une chaîne de caractères et qui la transforme en message codé sous forme
d'une suite d'idéogramme.

**Question 5.** Question ouverte et facultative : écrivez une fonction qui code un
fichier au contenu quelconque en un message codé sous forme
d'idéogrammes codés sur 3 octets.

**Question 6.**

-   Quel problème rencontre-t-on si on veut coder le message en
    utilisant des caractères codés en UTF-8 sur 4 octets ?
-   Est-ce que ce problème est insurmontable ?
