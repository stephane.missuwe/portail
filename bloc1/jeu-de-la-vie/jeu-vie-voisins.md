Trouver ses voisins dans le jeu de la vie
=========================================

Les codes Python fournis u-ici sont disponibles dans [jeu-de-la-vie.py](jeu-vie-voisins.py).

# 1. Méthode naïve

Pour trouver les valeurs contenues dans les cases voisines d'une case de
coordonnées `(x,y)` dans le jeu de la vie, il faut vérifier que les
coordonnées *autour* de `(x,y)` sont bien dans la grille.

Une méthode simple consiste à essayer de tester si chaque *voisin
potentiel* est bien sur la grille et lorsque c'est le cas d'ajouter la
valeur qu'il contient à la liste résultat.

```python
def voisins_case(g, x, y):
    """Renvoie la liste des valeurs contenues dans les cases voisines
    de la case de coordonnées (x,y) dans la grille g
    >>> voisins_case([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 1, 1) == [0, 1, 0, 1, 0, 1, 1, 1]
    True
    >>> voisins_case([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 2, 2) == [0, 0, 1]
    True
    >>> voisins_case([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 0, 2) == [1, 0, 0]
    True
    """
    voisins=[]                                             
    vx = x-1                                                   
    vy = y-1                                                 
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                                     
    vx = x-1                                                        
    vy = y                                                          
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                                     
    vx = x-1                                                        
    vy = y+1                                                        
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    vx = x                                                     
    vy = y-1                                                   
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    vx = x                                                   
    vy = y+1                                                 
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    vx = x+1                                                   
    vy = y-1                                                   
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    vx = x+1                                                   
    vy = y                                                     
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    vx = x+1                                                   
    vy = y+1                                                   
    if 0 <= vx and vx < hauteur(g) and 0 <= vy and vy < largeur(g): 
        voisins = voisins + [g[vx][vy]]                          
    return voisins
```
	
Ce code est assez long à écrire, mais en utilisant habilement les
copier/coller et quelques changement, on parvient à l'écrire.

Le code est cependant assez difficile à lire, car long. Analysons le :

1.  La fonction commence à initialiser la variable `voisins` par une
    liste vide. Cette variable contiendra le résultat en fin de calcul.

2.  Ensuite, la fonction parcourt tous les *voisins possibles* en fixant
    leur coordonnée *x* et *y* et *teste* si celui-ci est bien dans la
    grille `g`, dans ce cas il l'*ajoute* au contenu de `voisins`. 
 
3.  Enfin la fonction renvoie la liste `voisins` des valeurs collectées.

Un code difficile à lire pose plusieurs difficultés :

1.  Si il ne fonctionne pas, il est difficile de trouver pourquoi. Dans
    un code comme celui-ci obtenu par copier/coller, il fort probable
    que la correction d'une erreur nécessite de nombreuses modification.

2.  Si on doit le modifier ultérieurement (parce que, par exemple, on
    veut modifier notre jeu de la vie), il faut relire et comprendre le
    code et cela peut poser des difficultés.

L'utilisation du copier/coller pour écrire un programme est souvent le
signe que l'on a raté une idée, une généralisation possible.

Une première chose que l'on peut faire est d'utiliser une fonction qui
teste si des coordonnées désignent bien une case d'une grille.

```python
def est_dans_grille(g,x,y):
    """Vérifie que les coordonnées (x,y) correspondent à une case de la grille g.
    >>> est_dans_grille([[0,0,1],[1,1,0]],0,1)
    True
    >>> est_dans_grille([[0,0,1],[1,1,0]],4,1)
    False
    >>> est_dans_grille([[0,0,1],[1,1,0]],1,4)
    False
    >>> est_dans_grille([[0,0,1],[1,1,0]],4,4)
    False
    """
    return 0 <= x and x < hauteur(g) and 0 <= y and y < largeur(g)
```
	
Nous pouvons modifier la fonction `voisins_case` en utilisant
`est_dans_grille`. Nous obtenons :

```python
def voisins_case_2(g, x, y):
    """Renvoie la liste des valeurs contenues dans les cases voisines
    de la case de coordonnées (x,y) dans la grille g
    >>> voisins_case_2([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 1, 1) == [0, 1, 0, 1, 0, 1, 1, 1]
    True
    >>> voisins_case_2([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 2, 2) == [0, 0, 1]
    True
    >>> voisins_case_2([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 0, 2) == [1, 0, 0]
    True
    """
    voisins=[]
    vx = x-1
    vy = y-1
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    vx = x-1
    vy = y
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    vx = x-1
    vy = y+1
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    vx = x
    vy = y-1
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    vx = x
    vy = y+1
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    vx = x+1
    vy = y-1
    if est_dans_grille(g,vx,vy): 
        voisins = voisins + [g[vx][vy]]
    vx = x+1
    vy = y
    if est_dans_grille(g,vx,vy): 
        voisins = voisins + [g[vx][vy]]
    vx = x+1
    vy = y+1
    if est_dans_grille(g,vx,vy):
        voisins = voisins + [g[vx][vy]]
    return voisins
```

Le code ne gagne pas vraiment en lisibilité et reste bien long. Nous
allons voir comment le factoriser plus encore.

# 2. Lister les directions des voisins

Les *voisins possibles* d'une cases de coordonnées `(x,y)` peuvent
s'obtenir en prenant des valeurs `dx` et `dy` dans l'ensemble {-1,0,1}
et en considérant les coordonnées `(x+dx,y+dy)`. Il faut néanmoins faire
attention à ne pas considérer `(x,y)` comme son propre voisin et éviter
de choisir une paire `(dx,dy)` égale à `(0,0)`.

On peut mettre cette idée en oeuvre en `Python` relativement simplement
en faisant parcourir aux variables `dx` et `dy` la liste `[-1,0,1]`.
Cela nous donne le code suivant:

```python
def voisins_case_3(g,x,y):
    """Renvoie la liste des valeurs contenues dans les cases voisines
    de la case de coordonnées (x,y) dans la grille g
    >>> voisins_case_3([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 1, 1) == [0, 1, 0, 1, 0, 1, 1, 1]
    True
    >>> voisins_case_3([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 2, 2) == [0, 0, 1]
    True
    >>> voisins_case_3([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 0, 2) == [1, 0, 0]
    True
    """
    voisins = []                                 
    mv = [-1,0,1]                                        
    for dx in mv:                                             
        for dy in mv:                                         
            if (dx != 0 or dy !=0) and est_dans_grille(g,x+dx,y+dy): 
                voisins = voisins + [g[x+dx][y+dy]]   
    return voisins
```

Le code devient beaucoup plus court et plus lisible !

1.  On initialise la liste `voisins` qui contiendra les valeurs des
    voisins trouvés.

2.  On utilise la variable `mv` pour stocker les déplacements
    élémentaires possibles dans les deux directions (verticale et
    horizontale).

3.  La variable `dx` parcourt les déplacements élémentaires possibles.

4.  La variable `dy` parcourt également les déplacements élémentaires
    possibles.

5.  On teste :

    1.  que `(dx,dy)` n'est pas `(0,0)`, et

    2.  que `(x+dx,y+dy)` est bien une coordonnée de la grille `g`

6.  Lorsque le test réussit, on ajoute la valeur du voisin au contenu de
    `voisins`

7.  On renvoie les valeurs collectées dans la liste `voisins`

# 3. Vers une notation plus mathématique 

`Python` propose une notation pour les listes proche de la notation
ensembliste utilisée en mathématiques. Cette notation permet de rendre
le programme encore plus court et lisible.

Pour représenter les paires `(dx,dy)` employées, nous aimerions pouvoir
définir l'ensemble suivant :
$$\{(dx,dy) \mid dx \in MV,\,dy\in MV,\,(dx,dy)\neq(0,0)\} \text{ avec } MV =\{-1,0,1\}$$

En utilisant la notation de liste en *compréhension* de `Python` cela
donne :

```python
mv = [-1,0,1]
directions = [(dx,dy) for dx in mv for dy in mv if (dx,dy) != (0,0)]
```

De la même manière on peut définir la listes des valeurs des voisins
d'une case. Cela nous donne une dernière implémentation de
`voisins_case`.

```python
def voisins_case_4(g, x, y):
    """Renvoie la liste des valeurs contenues dans les cases voisines
    de la case de coordonnées (x,y) dans la grille g
    >>> voisins_case_4([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 1, 1) == [0, 1, 0, 1, 0, 1, 1, 1]
    True
    >>> voisins_case_4([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 2, 2) == [0, 0, 1]
    True
    >>> voisins_case_4([[0, 1, 0], [1, 0, 0], [1, 1, 1]], 0, 2) == [1, 0, 0]
    True
    """
    mv = [-1,0,1]                                        
    directions = [(dx,dy) for dx in mv for dy in mv if (dx,dy) != (0,0)] 
    return [g[x+dx][y+dy] for (dx,dy) in directions if est_dans_grille(g,x+dx,y+dy)]
```

Voyons rapidement comment cela fonctionne :

1.  On initialise la liste des déplacements élémentaires.

2.  On calcule la liste des directions dans laquelle se trouvent les
    voisins possibles.

3.  On renvoie la liste des valeurs contenues dans les cases voisines en
    vérifiant au préalable qu'elles sont bien dans la grille.

<!-- eof -->
