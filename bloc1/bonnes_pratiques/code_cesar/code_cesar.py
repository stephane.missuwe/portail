# + des identificateurs plus lisibles
# + une fonction paramétrée
# + séparation du calcul et de l'affichage : le calcul peut être réutilisé
# + de petites fonctions dont le code est facile à lire et donc à faire évoluer
# + la décomposition du code est lpus explicite, les commentaires dans le code deviennent superflus
# + des constantes nommées, donc plus explicites et plus faciles à changer
# + une documentation permet la compréhension du rôle de chaque fonction et son utilisation 
# + des tests qui permettent une validation du code de chaque fonction, et en plus fournissent des exemples d'utilisation 

DEFAULT_DECALAGE = 1

CODE_a = ord('a')
CODE_z = ord('z')

def est_lettre(car):
    '''
    vaut vrai si et seulement si car est une  lettre minuscule
    :param (char): le caractère à tester
    :return (boolean): True ssi car est une lettre minuscule
    :CU: car est une chaine de taille 1
    
    >>> est_lettre('c')
    True
    >>> est_lettre('C')
    False
    >>> est_lettre('!')
    False
    '''
    return ord(car) >= CODE_a and ord(car) <= CODE_z

def code_lettre(lettre, decalage):
    '''
    fournit la decalage-ème lettre de l'alphabet après lettre considérant que la lettre 'a' suit la lettre 'z'
    :param lettre: (char) la lettre à décaler
    :param decalage: (int) la valeur du décalage
    :return: (char) la lettre décalée
    :CU: car est une chaine de taille 1
    
    >>> code_lettre('c',1) == 'd'
    True
    >>> code_lettre('c',25) 
    'b'
    '''
    return chr ( CODE_a + (ord(lettre) - CODE_a + decalage) % 26 )

def code_cesar_caractere(car, decalage = DEFAULT_DECALAGE):
    '''
    renvoie la lettre car décalée de decalage caractères, le caractère 'a' suivant le caractère 'z'
    si car ne représente pas une lettre, il est inchangé
    :param car: (char) le caractère à coder
    :param decalage: (int) la valeur du décalage
    :return: (char) la version codée de car
            
    >>> code_cesar_caractere('c',1) == 'd'
    True
    >>> code_cesar_caractere('c',25) == 'b'
    True
    >>> code_cesar_caractere('*',25)
    '*'
    '''
    if est_lettre(car):
        return code_lettre(car, decalage)
    else:
        return car

def code_cesar_mot(mot, decalage = DEFAULT_DECALAGE):
    '''
    fournit le mot obtenu en appliquant un code de César de decalage caractères à mot.
    On obtient le code en décalant chaque lettre de decalage caractères dans l'alphabet, les caractères qui ne sont pas des lettres sont inchangés.
    :param mot: (string) le mot à coder
    :param decalage: (int) le décalage à appliquer
    :return: (string) le mot codé
    
    >>> code_cesar_mot('czAéb',2) == 'ebAéd'      
    True
    '''
    result = ''
    for lettre in mot :
        result = result + code_cesar_caractere(lettre, decalage)
    return result


LONGUEUR_MOT_MIN = 3
def code_mot(mot, decalage = DEFAULT_DECALAGE):
    '''
    fournit le mot obtenu en appliquant un code de César de decalage caractères à mot si sa longueur est au moins de LONGUEUR_MOT_MIN,
    sinon mot est inchangé
    :param mot: (string) le mot à coder
    :param decalage: (int) le décalage à appliquer
    :return: (string) le mot codé s'il a au moins LONGUEUR_MOT_MIN caractères, mot sinon
    
    >>> code_mot('czAéb',2)
    'ebAéd'
    >>> code_mot('ab',2)
    'ab'
    '''
    if len(mot) < LONGUEUR_MOT_MIN:
        return mot
    else:
        return code_cesar_mot(mot, decalage)


def code_phrase(phrase, decalage = DEFAULT_DECALAGE):
    '''
    fournit la phrase obtenue en appliquant un code de César à chacun des mots de la phrase
    :param phrase: (string) la phrase à coder
    :param decalage: (int) le décalage à appliquer
    :return: (string) la phrase codée
    
    >>> code_phrase('voici un test',2)
    'xqkek un vguv'
    '''
    liste_mots = phrase.split()
    liste_code = []
    for mot in liste_mots :
        liste_code.append(code_mot(mot,decalage))    
    return ' '.join(liste_code)


def affiche_code_phrase(phrase, decalage = DEFAULT_DECALAGE):
    '''
    affiche la phrase et sa version codée par un code de César de decalage caractères
    :param phrase: (string) la phrase à coder
    :param decalage: (int) le décalage à appliquer
    :return: None
    :effet de bord: affiche la phrase et sa version codée sur la sortie standard
    
    >>> affiche_code_phrase('voici un test',2)
    voici un test -> xqkek un vguv
    
    #voici un test   ->          xqkek un vguv       # pour doctest.NORMALIZE_WHITESPACE
    #voici un ...                                    # pour doctest.ELLIPSIS
    '''
    print( phrase + ' -> ' +   code_phrase(phrase, decalage) )
    



# il serait peut-être (certainement) pertinent de déplacer ce qui suit dans un fichier à part "code_cesar_main.py" par exemple

DEFAULT_PHRASE = "Aujourd'hui, est le troisieme jour du DIU."

def gere_argument():    
    phrase = DEFAULT_PHRASE
    decalage = DEFAULT_DECALAGE
    if len(sys.argv) > 1:
        phrase = sys.argv[1]
    if len(sys.argv) > 2:
        decalage = int(sys.argv[2])
    return (phrase, decalage)


if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    
    import sys
    (phrase, decalage) = gere_argument()
    
    affiche_code_phrase(phrase, decalage)
 

#
#
# gestion des erreurs d'arguments
"""
def usage():
    print('''
    usage : python3 code_cesar ["phrase à coder"] [decalage (int)]
      phrase par défaut = "Aujourd'hui, est le troisieme jour du DIU."
      décalage par défaut = 1
    exemples : 
      python3 code_cesar.py
      python3 code_cesar.py "Un message à coder."
      python3 code_cesar.py "Un message à coder." 13      
    ''')    

if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    import sys
    
    try:
        (phrase, decalage) = gere_argument()
    except :
        usage()
        sys.exit()

    affiche_code_phrase(phrase, decalage)
"""

    