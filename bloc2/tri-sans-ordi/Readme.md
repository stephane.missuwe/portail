Tri sans ordinateur
===================

_(Le descriptif de la séance et les ressources seront mises à jour
après la séance.)_

<!-- 
[Déroulé de la séance](deroule.md)
-->

## Matériel utilisé

Pour info, voici l'origine (1er semestre 2019) et la composition du
matériel utilisé : 

* pots à vis 25ml (31x40mm)  ref BUR-62140025  https://www.laboandco.com/  (120€ TTC pour 250 pots)
* écrous : Cergy Vis https://www.cergy-vis.fr/  (72€ TTC pour 500 écrous M8 et 200 M14)

Ces 2 fournisseurs acceptent les BC administratifs (les contacter par mail ou tel) : bon à savoir pour vos gestionnaires d'établissement 😉

À titre indicatif, pour une série de 10 poids distincts, nous avons
utilisé 20 écrous M8 et 7 M14. 
